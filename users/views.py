from django.http import JsonResponse
from django.contrib.auth import get_user_model
from rest_framework import generics
from rest_framework import status
from rest_framework.views import APIView
from rest_framework.generics import UpdateAPIView
from rest_framework import authentication, permissions
from rest_framework.response import Response
from .serializers import UserSerializer, ChangePasswordSerializer

CustomUser = get_user_model()


class UsersListView(generics.ListAPIView):
    serializer_class = UserSerializer
    queryset = CustomUser.objects.all()


class UserLearningPathDataView(APIView):
    permission_classes = [permissions.IsAuthenticated]

    def get(self, request, format=None):
        """
        Return a user's learning path data
        """
        response = JsonResponse({
            "paths": [{
                "id": 10302,
                "completed": 2,
                "status": "In Progress",
                "completion_percentage": 20,
                "units": [{
                                "id": 10352,
                                "name": "Agile Methoden",
                                "category": "E_LEARNING",
                                "provider": "Haufe Academy",
                                "start_date": "2020-02-22",
                                "end_date": "null",
                                "planned_start_date": "",
                                "planned_end_date": "",
                                "status": "COMPLETED",
                                "duration": "2 min",
                                "feedback_questions": [{
                                    "id": 11802,
                                    "text": "Was fandest Du gut?"
                                },
                                    {
                                    "id": 11803,
                                    "text": "Was würdest Du ändern?"
                                },
                                    {
                                    "id": 11804,
                                    "text": "Wie fandest Du den Inhalt? "
                                }
                                ]
                                },
                          {
                    "id": 10353,
                    "name": "Tools und Methoden - agiles Arbeiten",
                    "category": "SEMINAR",
                                "provider": "Pink University",
                                "start_date": "2020-02-25",
                                "end_date": "null",
                                "planned_start_date": "",
                                "planned_end_date": "",
                                "status": "WAITING_FOR_FEEDBACK",
                                "duration": "5 min",
                                "feedback_questions": [{
                                    "id": 11802,
                                    "text": "Was fandest Du gut?"
                                },
                                    {
                                    "id": 11803,
                                    "text": "Was würdest Du ändern?"
                                },
                                    {
                                    "id": 11804,
                                    "text": "Wie fandest Du den Inhalt? "
                                }
                                ]
                }
                ]
            }]
        })
        return response


class UserDetailView(APIView):
    permission_classes = [permissions.IsAuthenticated]

    def get(self, request, pk, format=None):
        """
        Returns a user's specific basic profile
        """
        response = JsonResponse({
            "id": 10412,
            "full_name": "David Topf",
            "department": "Manufacturing",
            "organization": "Peers",
            "profile_image": "https://peers-dev.s3.eu-west-1.amazonaws.com/users/photos/3380a070-475b-4bd3-8eaa-79d05753c350-Bewerbungsfoto_-_Square.png?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Date=20200618T130316Z&X-Amz-SignedHeaders=host&X-Amz-Expires=345600&X-Amz-Credential=AKIAJC5ZDLZOIUIJIBSQ%2F20200618%2Feu-west-1%2Fs3%2Faws4_request&X-Amz-Signature=4143cb832b6bdf755c0fa3817401067008922684504d6ce3b25dab0029b1dc41",
            "phone_number": "087654432",
            "groups": [
                "ROLE_USER",
                "ROLE_MANAGER",
                "ROLE_ADMIN"
            ],
            "job_title": "Senior Manager"
        })
        return response


class UsersLearningUnitDetailView(APIView):
    permission_classes = [permissions.IsAuthenticated]

    def get(self, request, pk, format=None):
        """
        Returns the details of learning unit to be accessed by a user.
        """
        response = JsonResponse({
            "id": 10352,
            "name": "Agile Methoden",
            "category": "E_LEARNING",
            "start_date": "2020-02-22",
            "end_date": "2020-10-25",
            "planned_start_date": "",
            "planned_end_date": "",
            "status": "COMPLETED",
            "content": "",
            "rating": "",
            "no_of_completed_users": "",
            "assets": "",
            "partner": "",
            "link": "",
            "duration": "2 min",
            "feedback_questions": [{
                "id": 11802,
                "text": "Was fandest Du gut?"
            },
                {
                "id": 11803,
                "text": "Was würdest Du ändern?"
            },
                {
                "id": 11804,
                "text": "Wie fandest Du den Inhalt? "
            }
            ]
        })
        return response

        def patch(self, request, pk, format=None):
            """
            Updates the status of a learning unit.
            """
            return JsonResponse(code=201, data="Succesfully updated the data.")


class UsersLearningUnitFeedbackAPI(APIView):
    def post(self, pk, request, format=None):
        '''
        Receieves the answers to the feedback
        questions and stores the feedback.
        '''
        return JsonResponse(code=201, data="Succesfully updated the data.")


class ChangePasswordView(UpdateAPIView):
    """
    An endpoint for changing password.
    """
    serializer_class = ChangePasswordSerializer
    model = get_user_model()
    permission_classes = [permissions.IsAuthenticated]

    def get_object(self, queryset=None):
        obj = self.request.user
        return obj

    def update(self, request, *args, **kwargs):
        self.object = self.get_object()
        serializer = self.get_serializer(data=request.data)

        if serializer.is_valid():
            # Check old password
            if not self.object.check_password(serializer.data.get("old_password")):
                return Response({"old_password": ["Wrong password."]}, status=status.HTTP_400_BAD_REQUEST)
            # set_password also hashes the password that the user will get
            self.object.set_password(serializer.data.get("new_password"))
            self.object.save()
            response = {
                'status': 'success',
                'code': status.HTTP_200_OK,
                'message': 'Password updated successfully',
                'data': []
            }

            return Response(response)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class OrganizationDetailView(APIView):
    """
    An endpoint to get details about user's 
    current organization
    """

    def get(self, request):
        response = JsonResponse({
            "name": "Riguzzi",
            "industry": "Steel",
            "address": "PreizenStr. 25, Berlin, 12345, Germany",
            "e-mail": "contact@riguzzi.de",
            "total_budget": 25000,
            "available_budget": "1000",
            "point_of_contact": "Dr. Vlad",
            "logo": "Riguzzi.jpg"
        })
        return response
