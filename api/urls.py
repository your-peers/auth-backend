from django.urls import path
from rest_framework_simplejwt.views import TokenObtainPairView, TokenRefreshView
from users.views import UsersListView, UserDetailView, UserLearningPathDataView, UsersLearningUnitDetailView, ChangePasswordView, UsersLearningUnitFeedbackAPI, OrganizationDetailView


urlpatterns = [
    path('token/', TokenObtainPairView.as_view(), name='create-token'),
    path('token/refresh/', TokenRefreshView.as_view(), name='refresh-token'),
    path('user/organization/', OrganizationDetailView.as_view(),
         name='get-organization'),
    path('users/', UsersListView.as_view(), name='get-all-users'),
    path('users/change-password/',
         ChangePasswordView.as_view(), name='change-password'),
    path('users/learning-paths/', UserLearningPathDataView.as_view(),
         name='get-users-learning-path'),
    path('users/learning-units/<int:pk>/',
         UsersLearningUnitDetailView.as_view(), name='users-learning-unit'),
    path('users/<int:pk>/', UserDetailView.as_view(), name='get-specific-user'),
    path('users/learning-units/<int:pk>/feedback/',
         UsersLearningUnitFeedbackAPI.as_view(), name='users-learning-unit-feedback'),

]
