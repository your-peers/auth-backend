# Auth Sandbox Backend - Peers

API for login/logout, create access/refresh token pair and update the access token with refresh token.

## Usage Instructions

Please install Docker if you don't have that in the machine.

1. Run this command in your terminal to start the services in a detached mode:
   `docker-compose up -d --build`

2. Run this command after that
   `docker-compose exec web python manage.py createsuperuser`

And enter an username, password and e-mail address to create a user account.

3. After creating an account, try the APIs and the detailed documentation is available here [API Documentation](https://peers-solutions.atlassian.net/l/c/HU44QY0q)
